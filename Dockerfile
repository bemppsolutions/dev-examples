# Distributed under the terms of the MIT License
# This file and the associated scripts have been adapted from
# the Jupyter Notebook docker files available at https://github.com/jupyter/docker-stacks, and licensed under the Modified BSD License.

From bempp/base:latest

Maintainer Timo Betcke <timo.betcke@gmail.com>

USER root

RUN apt-get update && apt-get -y install \
    dh-make \
    gcc \
    g++ \
    gfortran \
    libeigen3-dev \
    python3-dev \
    python3-numpy \
    patchelf \
    libtbb-dev \
    zlib1g-dev \
    libboost-all-dev \
    cmake \
    cpio \
    libdune-common-dev \
    libdune-geometry-dev \
    libdune-grid-dev \
    libdune-localfunctions-dev \
    mpi-default-dev \
    cython3 \
    python3-setuptools \
    python3-mpi4py \
    python3-scipy \
    python3-tk \
    dh-python \
    python3-all \
    python3-pip \
    libpython3.5 \
    libtbb2 \
    libstdc++6 \
    vim \
    gmsh \
    git \
    && apt-get clean

RUN add-apt-repository -y ppa:bemppsolutions/bempp-dev
RUN apt-get update && apt-get -y install python-bempp python3-bempp
RUN apt-get clean

###### uwsgi and nginx stuff

RUN pip3 install uwsgi

# Standard set up Nginx
ENV NGINX_VERSION 1.9.11-1~xenial

RUN apt-key adv --keyserver hkp://pgp.mit.edu:80 --recv-keys 573BFD6B3D8FBC641079A6ABABF5BD827BD9BF62 \
	&& echo "deb http://nginx.org/packages/mainline/ubuntu/ xenial nginx" >> /etc/apt/sources.list \
	&& apt-get update \
	&& apt-get install -y ca-certificates nginx gettext-base \
	&& rm -rf /var/lib/apt/lists/*

# forward request and error logs to docker log collector
RUN ln -sf /dev/stdout /var/log/nginx/access.log \
	&& ln -sf /dev/stderr /var/log/nginx/error.log
EXPOSE 80 443
# Finished setting up Nginx

# Make NGINX run on the foreground
RUN echo "daemon off;" >> /etc/nginx/nginx.conf
# Remove default configuration from Nginx
RUN rm /etc/nginx/conf.d/default.conf
# Copy the modified Nginx conf
COPY nginx.conf /etc/nginx/conf.d/
# Copy the base uWSGI ini file to enable default dynamic uwsgi process number
COPY uwsgi.ini /etc/uwsgi/

# Install Supervisord
RUN apt-get update && apt-get install -y supervisor \
&& rm -rf /var/lib/apt/lists/*
# Custom Supervisord config
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf

##### FLASK AND APP

RUN sudo pip3 install flask flask-restful mako

# By default, allow unlimited file sizes, modify it to limit the file sizes
# To have a maximum of 1 MB (Nginx's default) change the line to:
# ENV NGINX_MAX_UPLOAD 1m
ENV NGINX_MAX_UPLOAD 0

# Which uWSGI .ini file should be used, to make it customizable
ENV UWSGI_INI /app/uwsgi.ini

# URL under which static (not modified by Python) files will be requested
# They will be served by Nginx directly, without being handled by uWSGI
ENV STATIC_URL /static
# Absolute path in where the static files wil be
ENV STATIC_PATH /app/static

# If STATIC_INDEX is 1, serve / with /static/index.html directly (or the static URL configured)
# ENV STATIC_INDEX 1
ENV STATIC_INDEX 0


# Copy the entrypoint that will generate Nginx additional configs
COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]


COPY ./app /app
WORKDIR /app

COPY ./config_py /config_py

CMD ["/usr/bin/supervisord"]
# ENV NB_USER bempp
# ENV NB_UID 1000
# ENV SHELL /bin/bash
# ENV HOME /home/$NB_USER
# ENV LC_ALL en_US.UTF-8
# ENV LANG en_US.UTF-8
# ENV LANGUAGE en_US.UTF-8

# RUN mkdir /home/$NB_USER/bempp && \
#     echo "cacert=/etc/ssl/certs/ca-certificates.crt" > /home/$NB_USER/.curlrc

# USER root
# EXPOSE 8888
# WORKDIR /home/$NB_USER/bempp
#CMD ["/bin/bash"]

# RUN echo '#!/bin/bash\nset -e\njupyter notebook $*\n' >> /usr/local/bin/start-notebook.sh && chmod 755 /usr/local/bin/start-notebook.sh

# RUN echo "c = get_config()\nc.NotebookApp.ip = '*'\nc.NotebookApp.port = 8888\nc.NotebookApp.open_browser = False\n" >> /home/$NB_USER/.jupyter/jupyter_notebook_config.py && chown -R $NB_USER:users /home/$NB_USER/.jupyter

# USER $NB_USER

