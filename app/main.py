from flask import Flask, request
from flask_restful import Resource, Api

import os
import json
import subprocess

app = Flask(__name__)
api = Api(app)

class HelloWorld(Resource):
    def get(self):
        return {'hello': 'world'}

VALUES = {}
class RunCode(Resource):
    def put(self, name):
        if name in VALUES.keys():
            return {name: "Job already exists"}

        json_data = request.get_json(force=True)
        VALUES[name] = dict()
        VALUES[name]['frequency'] = json_data['frequency']
        VALUES[name]['incident_angle'] = json_data['incident_angle']
        VALUES[name]['speed_of_sound'] = json_data['speed_of_sound']

        # write json file
        VALUES[name]['grid_file'] = "cube.json"
        VALUES[name]['solver_tolerance'] = 1e-5
        VALUES[name]['number_of_evaluation_points'] = 400

        # Maybe permissions? - lets create the folders first.
        directories = "/tmp/bempp/config"
        os.makedirs(directories, exist_ok=True)
        with open(directories + '/osrc_burton_miller.json', 'w') as f:
            f.write(json.dumps(VALUES[name]))
        # run generate.py
        import sys
        sys.path.append('/config_py')
        from prebempp import generate
        generate.main("/tmp/")
        # # run code
        DETACHED_PROCESS = 0x00000008
        VALUES[name]['process'] = subprocess.Popen(['python3', 'osrc_burton_miller.py'],
                                               cwd='/tmp/bempp/build/',
                                               stdout=subprocess.PIPE)
        VALUES[name]['pid'] = VALUES[name]['process'].pid
        # FIXME: It's just not finishing...
        output = ['frequency', 'incident_angle', 'speed_of_sound', 'grid_file', 'solver_tolerance', 'number_of_evaluation_points', 'pid']
        return {name: {i:VALUES[name][i] for i in output}}
        #return {name: VALUES[name], 'file': ",".join([val for sublist in [[os.path.join(i[0], j) for j in i[2]] for i in os.walk('./tmp/bempp/')] for val in sublist])}

    def get(self, name):
        output = ['frequency', 'incident_angle', 'speed_of_sound', 'grid_file', 'solver_tolerance', 'number_of_evaluation_points', 'pid']
        return {name: {i:VALUES[name][i] for i in output}}

class Status(Resource):
    def get(self, name):
        # TODO: Job exists
        if name not in VALUES.keys():
            return {name: "Job doesn't exists"}
        p = VALUES[name]['process']
        if p.poll() is None:
            return {name: "Running"}
        else:
            return {name: "Completed"}


class Cancel(Resource):
    def get(self, name):
        # TODO: Job exists?
        if name not in VALUES.keys():
            return {name: "Job doesn't exists"}
        p = VALUES[name]['process']
        p.terminate()
        del VALUES[name]
        return {name: "Cancelled"}

class Result(Resource):
    def get(self, name):
        if name not in VALUES.keys():
            return {name: "Job doesn't exists"}
        p = VALUES[name]['process']
        if p.poll() is None:
            return {name: "Job not complete"}
        #TODO: package all the results
        return {name: "Results available"}
    # FIXME: This is not what I thought... we need stop as an input
    # def stop(self, name):
    #     import psutil
    #     p = psutil.Process(VALUES[name]['pid'])
    #     p.terminate()

api.add_resource(HelloWorld, '/')
api.add_resource(RunCode, '/run/<string:name>') #'/<string:name>/run/<string:name>')
api.add_resource(Status, '/status/<string:name>')
api.add_resource(Cancel, '/cancel/<string:name>')
api.add_resource(Result, '/results/<string:name>')

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True, port=80)


# from flask import Flask, request
# from flask_restful import Resource, Api

# app = Flask(__name__)
# api = Api(app)

# todos = {}

# class TodoSimple(Resource):
#     def get(self, todo_id):
#         return {todo_id: todos[todo_id]}

#     def put(self, todo_id):
#         todos[todo_id] = request.form['data']
#         return {todo_id: todos[todo_id]}

# api.add_resource(TodoSimple, '/<string:todo_id>')

# if __name__ == '__main__':
#     app.run(host='0.0.0.0', debug=True, port=80)

