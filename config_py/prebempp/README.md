Contains example job for developing the online platform

The example templates are in the 'templates' subdirectory.
Corresponding config files are in the 'config' subdirectory.

Build the templates with

>> python generate.py

The resulting Python scripts are in the 'build' subdirectory.

