from mako.template import Template
import json
import os
import glob
import shutil

def main(dir_path=None):
    file_names = ['laplace_interior_dirichlet',
                  'maxwell_screen',
                  'osrc_burton_miller']

    if not dir_path:
        dir_path = os.path.dirname(os.path.realpath(__file__))

    template_path = '/config_py/prebempp/templates/'
    grid_path = '/config_py/prebempp/grids/'

    config_path = dir_path + '/bempp/config/'
    output_path = dir_path + '/bempp/build/'

    if not os.path.exists(output_path):
        os.makedirs(output_path)

    for f in glob.glob(output_path + '*'):
        os.remove(f)

    for f in glob.glob(grid_path + '*'):
        shutil.copy(f, output_path)


    for name in file_names:
        if not os.path.exists(config_path + name + ".json"):
            continue
        infile = template_path + name + '.py.in'
        outfile = output_path + name + '.py'
        configfile = config_path + name + '.json'
        with open(configfile, 'r') as f:
            params = json.load(f)
        with open(outfile, 'w') as f:
            f.write(Template(filename=infile).render(**params))
